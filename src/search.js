const request = require('request');
const cheerio = require('cheerio');

var maxPage, collectedLinks = [],
  currentPage = 0,
  mainCallback, limit, term;

function search(trm, lim, callback) {
  collectedLinks = [];
  currentPage = 0;
  limit = lim;
  term = trm;
  getLinks('https://www.amazon.com/s/ref=a9_asi_1?keywords=' + term + '&ie=UTF8', (links, $) => {
    maxPage = parseInt($('.pagnDisabled').eq(0).text());
    mainCallback = callback;
    handleResponse(links, $);
  });
}

function handleResponse(links, $) {
  ++currentPage;
  collectedLinks = collectedLinks.concat(links);
  if (collectedLinks.length >= limit || currentPage >= maxPage) {
    return mainCallback(collectedLinks);
  }
  getLinks(buildUrl($), handleResponse);
}

function buildUrl($) {
  var url = 'https://www.amazon.com/s/ref=sr_pg_2?fst=p90x%3A1&rh=i%3Aaps%2Ck%3A' + term + '&page=' + currentPage + '&keywords=' + term + '&ie=UTF8&qid=1494924641&spIA=1785882260,1785285025';
  return url;
}

function getLinks(url, linksCollectedCallback) {
  request.get({
    url: url,
    jar: true,
    headers: {
      'Host': 'www.amazon.com',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:53.0) Gecko/20100101 Firefox/53.0',
      'Accept': '*/*'
    }
  }, (error, httpRequest, body) => {
    var $ = '';
    try {
      $ = cheerio.load(body);
      var all = $('a.s-access-detail-page');
      var items = [];
      all.each(function (i, a) {
        if (a.attribs.href.indexOf('picassoRedirect') === -1) {
          items.push(a.attribs.href);
        }
      });
    } catch (e) {
      console.log(e);
    }
    linksCollectedCallback(items, $);
  });
}

module.exports = search;