
var fs = require('fs');


function getProxy() {
    var proxies = fs.readFileSync('proxies.txt').toString().split('\n');
    if (proxies.length > 0) {
        var proxy = proxies[Math.floor(Math.random() * proxies.length)];
        if (proxy.indexOf(':') > 0) {
            return proxy;
        }
    }
    return null;
}

module.exports = getProxy;