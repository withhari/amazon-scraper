var fs = require('fs');
var captchaSolver = require('./captcha');

function getFromFile(filename) {
    var lines = fs.readFileSync(filename).toString().split('\n');
    if (lines.length > 0) {
        var line = lines[Math.floor(Math.random() * lines.length)]
        if (line.trim().length == 0 && lines.length > 1) {
            return getFromFile(filename);
        }
        return line;
    }
    return '';
}

function gotoProfiles(profiles, oneDoneCalback, allDoneBack) {
    var options = {
        show: true,
        jar: true,
        waitTimeout: 30000,
        webPreferences: {
            images: false
        }
    }
    var proxy = getFromFile('proxies.txt');
    if (proxy.indexOf(':') > 0) {
        options.switches = {
            'proxy-server': proxy,
            'ignore-cerificates-error': true
        }
        console.log('Using proxy: ' + proxy);
    }
    var nightmare = require('nightmare')(options);
    var emails = [];
    var count = 0;
    login();
    function login() {
        var account = getFromFile('auths.txt').split(':');
        var email = account[0];
        var password = account[1];
        nightmare
            .useragent('Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30')
            .goto('https://www.amazon.com/gp/aw/so.html?action=sign-out&redirectTo=/gp/aw/si.html')
            .wait('form[name="signIn"]')
            .type('#ap_email', email)
            .type('#ap_password', password)
            .click('#signInSubmit')
            .wait('input[name="k"]')
            .then(function (err) {
                if (err) {
                    checkCaptcha();
                } else {
                    console.log('~Login');
                    gotoNext();
                }
            })
            .catch(function (error) {
                if (error) {
                    console.log('Main Error: ' + error.message || error);
                    checkCaptcha();
                }
            });
    }

    function typeCaptcha(text) {
        nightmare
            .evaluate(function (t) {
                document.getElementById('captchacharacters').value = t;
                document.forms[0].submit();
                return true;
            }, text)
            .then(() => {
                gotoNext();
            })
            .catch(e => {
                if (e) {
                    endInstance();
                }
            })
    }
    
    function checkCaptcha() {
        nightmare
            .evaluate(function() {
                var imgs = document.querySelectorAll('img');
                for(var i = 0; i < imgs.length; i++) {
                    if (imgs[i].href.toLowerCase().indexOf('captcha') > 0) {
                        return imgs[i];
                    }
                }
                return '';
            })
            .then(function(captchaLink) {
                if (captchaLink.length > 0) {
                    //captchacharacters
                    captchaSolver(captchaLink, function (err, text) {
                        if (err) {
                            login();
                        } else {
                            typeCaptcha(text);
                        }
                    })
                } else {
                    login();
                }
            })
            .catch(e => {
                endInstance();
            })
    }

    function endInstance() {
        nightmare.end();
        nightmare.proc.disconnect();
        nightmare.proc.kill();
        nightmare.ended = true;
        nightmare = null;
        allDoneBack(profiles, emails)
    }

    function updateCount(profile) {
        count++;
        if (arguments.length >= 2) {
            //see if there's any error messages
            console.log(arguments[1]);
        }
        oneDoneCalback(profile.index);
        if (count >= profiles.length) {
            endInstance();
        } else {
            gotoNext();
        }
    }

    function gotoNext() {
        if (profiles.length == 0) {
            return updateCount({ index: -1 });
        }
        var profile = profiles.pop();
        if (profile.link == null || profile.link.length == 0) {
            return updateCount(profile);
        }
        console.log(profile.link);
        nightmare
            .goto(profile.link)
            .wait('#profile-v5-mob-vis-layout')
            .evaluate(function () {
                return document.getElementById('profile-v5-mob-vis-layout').getAttribute('data');
            })
            .then(function (data) {
                try {
                    data = JSON.parse(data).bioData;
                    if (data.publicEmail && data.publicEmail.length > 0) {
                        emails.push(data.publicEmail);
                    }
                    if (data.personalDescription && data.personalDescription.indexOf('@') > 0) {
                        var tmpEmails = data.split(/[\s,}{"]+/).filter(function (word) {
                            return /\S+@\S+\.\S+/.test(word);
                        });
                        emails = emails.concat(tmpEmails);
                    }
                } catch (ignore) {
                    console.log(ignore);
                }
                updateCount(profile);
            })
            .catch(function (err) {
                if (err) {
                    updateCount(profile, 'Calling after Error.');
                    console.log('1: ' + err.message || err);
                }
            });
    }
}

module.exports = gotoProfiles;